from peewee import *
from modelPlayer import *
from util import *



def load():
	return ToList(Player.select().dicts())



def loadById(idPlayer: int):
	ret = Player.select().where(Player.id == idPlayer)
	if(len(ret) > 0):
		return ret.dicts().get()
	else:
		return None




def create(player: Player):
	Player.create(name=player.name, password=player.password)



