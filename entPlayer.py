
from pydantic import BaseModel, Field
from typing import Optional


class Player(BaseModel):
	name: Optional[str] = None
	password: Optional[str] = None


