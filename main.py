from fastapi import FastAPI, HTTPException, Response, status
from fastapi.middleware.cors import CORSMiddleware

from casePlayer import *
from entPlayer import *
from util import *


app = FastAPI()


origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

#PLAYERS

@app.get("/Players")
async def getPlayers():
    return caseLoadPlayers()


@app.get("/Players/{idPlayer}")
async def getPlayers(idPlayer: int):
    return ValidateReturn(caseLoadPlayersById(idPlayer))



@app.post("/Players", status_code=status.HTTP_201_CREATED)
async def createGame(player: Player):
    return caseCreatePlayer(player)

