from fastapi import FastAPI, Response, status
from fastapi.responses import JSONResponse




def ToList(lista):
	ret = []
	for row in lista:
		ret.append(row)
	return ret



def ValidateReturn(obj):
	if(obj != None):
		return obj
	else:
		return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content={"message": "Item not found"})