from dalPlayer import *


def caseLoadPlayers():
	return load()
	

def caseLoadPlayersById(idPlayer: int):
	return loadById(idPlayer)
	

def caseCreatePlayer(player: Player):
	return create(player)

